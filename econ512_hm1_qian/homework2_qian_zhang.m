%% Homework 2 
clear;

%% Problem 1
v=[-1,-1,-1]'; 
p=[1,1,1]'; 
expvp=exp(v-p); %parameters and model setting

q=expvp./(1+sum(expvp)); %demand functions as vectors

P1_Q=[q
    1-sum(q)]; %final result

disp('Problem 1');
disp(P1_Q);
disp('------------------');

%% Problem 2
disp('Problem 2')
addpath('\CEtools');

optset('broyden','showiters',100);
optset('broyden','maxit',100) ;
optset('broyden','tol',1e-6) ;
%optset according to the broyden.m

P2_P=zeros(4,3);

P0=[1,1,1
    0,0,0
    0,1,2
    3,2,1];
%initial values 

tic
for i=1:4 
    P=P0(i,:)'; %for every line of initial guesses, run the broyden function
    [P,pfval]=broyden(@(P) pval(P,v),P); %directly apply broyden function   
    P2_P(i,:)=P'; %bertrand equilibrium prices
    
    disp('Inital value:');
    disp(P0(i,:));
    disp('the Bertrand equilibrium:')
    disp(P2_P(i,:));
    disp('convergence criteria:');
    disp('maxmum interation:100');
    disp('tolerance:1e-6');

end
toc

disp('------------------');

%% Problem 3
disp('Problem3');

tol=1e-6; % good to use the same tolerance, but you usulally use 1e-8
maxit=100; %tolerance level and max iteration all the same for every problem
P3_P=zeros(4,3);

tic
for i=1:4
    it=0;
    pold=P0(i,:)'; %initial guess for every line
    pnew=0.9*pold+0.0001; %slight change every iteration
    P=pold;
    while it<=maxit &&  tol<max(abs(pnew-pold))/abs(1+max(pold))
        %if pnew and pold are large enough, run the iteration again
        pold=pnew;
        pnew=P;
        delta_f=(pval(pnew,v)-pval(pold,v))./(pnew-pold); 
        %first order derivation of demand function
        for j=1:3 %for the 3 prices, separately run the iteration 
            t=pval(P,v);
            P(j)=P(j)-1/delta_f(j)*t(j); %separate iteration
        end

        it=it+1 ;
    end
    
    Q3_P(i,:)=pnew';
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Bertrand equilibrium:')
    disp(pnew');
    

    
end
toc

disp('------------------');

%% Problem 4
disp('Problem 4');
tol=1e-6;
maxit=100;
P4_P=zeros(4,3);

tic
for i=1:4
    it=0;
    pold=P0(i,:);
    pnew=1000+P0(i,:);
    while it<=maxit &&  tol<max(abs(pnew-pold))/abs(1+max(pold))
        pold=pnew;
        expvp=exp(v'-pold);
        q=expvp./(1+sum(expvp));
        pnew=1./(1-q); %the main difference is the updating rule
        it=it+1;
    end
    
    Q4_P(i,:)=pnew;
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Bertrand equilibrium:')
    disp(pnew);
 
end
toc

disp('It is faster than Guass-Jacobi method.')
disp('------------------');

%% Problem 5
disp('Problem 5');
tol=1e-6;
maxit=100;
P4_P=zeros(4,3);

tic
for i=1:4
    it=0;
    pold=P0(i,:)';
    pnew=0.9*pold+0.0001;
    P=pold;
    while it<=maxit &&  tol<max(abs(pnew-pold))/abs(1+max(pold))
        
        pold=pnew;
        pnew=P;
        delta_f=(pval(pnew,v)-pval(pold,v))./(pnew-pold);
        for j=1:3    
            t=pval(P,v);
            P=P-t.*delta_f; 
   %the difference is that we run the price vector iteration simultaneously
        end

        it=it+1 ;
    end
    
    Q5_P(i,:)=pnew';
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Bertrand equilibrium:')
    disp(pnew');
    

    
end
toc

disp('------------------');