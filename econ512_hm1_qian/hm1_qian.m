%Question 1
x=[1 1.5 3 4 5 7 9 10];  
Y1=-2+0.5*x
Y2=-2+0.5*x.^2
plot(x,Y1,'DisplayName','Y1');hold on;plot(x,Y2,'DisplayName','Y2');hold off;

%Question 2
A=-10:30/199:20;  % To divide the interval equally into 200 intervals
A2=A' % This is a 200x1 vector
sum(A2)  % Sum up each elements of the vector

%Question 3
A3=[2 4 6; 1 7 5; 3 12 4]; 
B=[-2; 3; 10];
C=A3'*B  
D=(A3'*A3)^-1*B
T=[B';B';B'];  % In order to calculate the summation of ��_j??[a_ij?b_i]?
E=A3*T;   % The diagonal elements are the summations for each i
trace(E)  % Summing up the diagonal elements
F=A3([1,3],[1,2])  % Deleting Row 2 and Column 3

% Question 4
% check out kron() in the answer key
G=blkdiag(A3,A3,A3,A3,A3)  % Diagonal blocks

% Question 5
S=5.*randn(5,3)+10;  % Creating 5x3 matrix of random draws from normal distribution with mean=10 and SD=5
N = zeros(size(S));  % Make another array to fill up...
for ii = 1:numel(S)  % Conditions to satisfy
    if S(ii)>10
       N(ii) = 1;
    else
       N(ii) = 0;
    end
end

% Question 6
% Import data from datahw1.csv
% please write code in such a way that I odnt need to import data myself
tbl=table(VarName3, VarName4, VarName6, VarName5, 'VariableNames', {'Export', 'RD', 'cap', 'Prod'});  % Construct table of variables
OLS=fitlm(tbl,'Prod~Export+RD+cap')
