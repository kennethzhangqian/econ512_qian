function pfval=pval(p,v) %FOC functions

    expvp=exp(v-p);
    q=expvp./(1+sum(expvp)); %vector demand functions 
    pfval   = 1+p.*q-p; %the FOC condition for profit optimization
 
