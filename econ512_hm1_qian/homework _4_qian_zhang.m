%% Homework 4 Qian ZHANG

%close all
%clear all
clc

global N T X Y Z
global btaPrime_u w_u btaPrime_l w_l
global randBta_u randBta_l randBta0
global randbtamu0
load('hw4data.mat')

N = data.N;
T = data.T;
X = data.X;
Y = data.Y;
Z = data.Z;


%% Question 1

% First, divide the intervals into two cases: <0 and >=0;
% Second, transform the intervals to a finite one: bta=-log(bta') and
% bta=log(-bta'); Then the intervals for bta' become [-1 0] and [0 1];
% Third, use Guassian Quadrature to find the weights and notes
% Final calculate the results.
sigmabta2 = 1;
bta0 = 0.1;
Nodes = 20;
[btaPrime_u,w_u] = lgwt(Nodes,0,1); % find the weights and notes when bta>0
[btaPrime_l,w_l] = lgwt(Nodes,-1,0); % find the weights and notes when bta<0

Li_u = ones(N,Nodes);
Li_l = ones(N,Nodes);

for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epi=-log(btaPrime_u(iNodes))*X(iT,iN);
            t1 = exp(-epi);
            Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
            
            epi=log(-btaPrime_l(iNodes))*X(iT,iN);
            t2 = exp(-epi);
            Li_l(iN,iNodes) = Li_l(iN,iNodes)*(1/(1+t2))^Y(iT,iN)*...
                (t2/(1+t2))^(1-Y(iT,iN));
        end
        Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(2*pi*sigmabta2)^0.5)*exp(-(-log(btaPrime_u(iNodes))-bta0)^2/2/sigmabta2)/btaPrime_u(iNodes);
        Li_l(iN,iNodes) = Li_l(iN,iNodes)*(1/(2*pi*sigmabta2)^0.5)*exp(-(log(-btaPrime_l(iNodes))-bta0)^2/2/sigmabta2)/(-btaPrime_l(iNodes));
    end
end

Intr_u = Li_u*w_u;
Intr_l = Li_l*w_l;
lnIntr   =log( Intr_u+Intr_l);
lnL = sum(lnIntr);
fprintf('The likelihood is: %d when using Gaussian Quadrature.\n', lnL);

%% Question 2

% The initial steps are the same as Question 1;
% Final calculate the results using Monte Carlo.

sigmabta2 = 1;
bta0 = 0.1;
Nodes = 100;
rng(74323);
randBta_u=rand(N,Nodes);
randBta_l=rand(N,Nodes);
Li_u = ones(N,Nodes);
Li_l = ones(N,Nodes);

for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epi=-log(randBta_u(iN,iNodes))*X(iT,iN);
            t1 = exp(-epi);
            Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
            
            epi=log(randBta_l(iN,iNodes))*X(iT,iN);
            t2 = exp(-epi);
            Li_l(iN,iNodes) = Li_l(iN,iNodes)* (1/(1+t2))^Y(iT,iN)*...
                (t2/(1+t2))^(1-Y(iT,iN));
        end
        Li_u(iN,iNodes) = Li_u(iN,iNodes)* (1/(2*pi*sigmabta2)^0.5)*exp(-(-log(randBta_u(iN,iNodes))-bta0)^2/2/sigmabta2)/randBta_u(iN,iNodes);
        Li_l(iN,iNodes) = Li_l(iN,iNodes)* (1/(2*pi*sigmabta2)^0.5)*exp(-(log(randBta_l(iN,iNodes))-bta0)^2/2/sigmabta2)/(randBta_l(iN,iNodes));
    end
end

Intr_u = sum(Li_u,2)/Nodes;
Intr_l = sum(Li_l,2)/Nodes;
lnIntr   = log(Intr_u+Intr_l);
lnL = sum(lnIntr);
fprintf('The likelihood is: %d when using Monte Carlo Method', lnL);


%% Question 3

% Gaussian Quadrature CASE
lb = [-2 0 -2];
ub = [2 2 2];

Para0 =[0.1;1;0];

[Para_GQ,FVAL_GQ] = fmincon(@(Para) GaussianQuadrature(Para),Para0,[],[],[],[],lb,ub)
fprintf('When using Gaussian Quadrature:\n');
fprintf('The starting values are:                               %f,    %f,    %f\n', Para0);
fprintf('The Beta_0 is:                                         %f \n',Para_GQ(1));
fprintf('The SigmaBeta^2 is:                                    %f \n',Para_GQ(2));
fprintf('The Gamma is:                                          %f. \n',Para_GQ(3));
fprintf('The maximized value of the LOG likelihood funtion is:  %f\n', -FVAL_GQ);


% Monte Carlo Method CASE
[Para_MC1,FVAL_MC1]  = fmincon(@(Para) MonteCarloMethod1(Para),Para0,[],[],[],[],lb,ub)
fprintf('When using Monte Carlo:\n');
fprintf('The starting values are:                               %f,    %f,    %f\n', Para0);
fprintf('The Beta_0 is:                                         %f \n',Para_MC1(1));
fprintf('The SigmaBeta^2 is:                                    %f \n',Para_MC1(2));
fprintf('The Gamma is:                                          %f. \n',Para_MC1(3));
fprintf('The maximized value of the LOG likelihood funtion is:  %f\n', -FVAL_MC1);


%% Question 4


sigmabta2 = 1;
bta0 = 0.1;
gamma = 0;
mu0 =0.1;
sigmamu2=1;
sigmabtamu=0.1;

% generate normals, check correlations
rng(74323);
randbtamu0 = randn(100000,2);
corrcoef(randbtamu0);

% desired correlation
M =[sigmabta2^0.5  sigmabtamu
    sigmabtamu  sigmamu2^0.5  ];

% induce correlation, check correlations
C = chol(M);
randbtamu1 = randbtamu0 * C;
corrcoef(randbtamu1);
randbtamu1(:,1) = bta0+randbtamu1(:,1);
randbtamu1(:,2) = mu0+randbtamu1(:,2);
corrcoef(randbtamu1);


lb = [  -3  0   -3  -2  0   -1];
ub = [  5   5   3   2   3   1];

Para0 =[0.1,    1,  0,  0.1,    1,  0.1];

% Monte Carlo Method CASE : mu0~=0 
[Para_MC3,FVAL_MC3]  = fmincon(@(Para) MonteCarloMethod3(Para),Para0,[],[],[],[],lb,ub)
% MontecarloMethod3 is still not on the repository. it does not show up in
% my computer system
fprintf('When using Monte Carlo (mu0~=0):\n');
fprintf('The starting values are:                               %f,    %f,    %f    %f,    %f,    %f \n', Para0);
fprintf('The Beta_0 is:                                         %f \n',Para_MC3(1));
fprintf('The SigmaBeta^2 is:                                    %f \n',Para_MC3(2));
fprintf('The Gamma is:                                          %f. \n',Para_MC3(3));
fprintf('The mu0 is:                                            %f. \n',Para_MC3(4));
fprintf('The Sigmamu^2 is:                                      %f. \n',Para_MC3(5));
fprintf('The SigmaBetamu is:                                    %f. \n',Para_MC3(6));
fprintf('The maximized value of the LOG likelihood funtion is:  %f\n', -FVAL_MC3);







