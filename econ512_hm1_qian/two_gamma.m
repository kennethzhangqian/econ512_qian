function loglik = two_gamma(theta)
global n x Y1 Y2;
loglik = -(length(n)*theta(1)*log(theta(2)) + (theta(1) - 1) * ...
    log(Y2) - theta(2) * Y1 - sum(loggamma(theta(1))));
end