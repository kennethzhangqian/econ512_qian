clear all; clc;
clear global;


% randomly generate the data
rng(1)

global n x Y1 Y2;
% you don't need to make some varialbes global, just pass them to a
% function

n = 100;

x = random('Uniform', 0,1.5,n,1);  % generate data from lognormal
theta1_t = 1.5;  % true value of theta1
theta2_t = -0.5;   % true value of theta2
fx = ((theta2_t^theta1_t)*(x.^(theta1_t-1)).*(exp(-theta2_t*x)))./gamma(theta1_t);
                                                    % random generated y

Y1 = sum(x); % as defined in the question 
Y2 = exp(sum(log(x))); % as defined in the question 

opts_off=optimset('DerivativeCheck','off','Display','off','TolX',1e-6,'TolFun',1e-6,...
   'Diagnostics','off','MaxIter', 200, 'LargeScale','off');

[theta_prime,loglik_prime] = fminsearch ('two_gamma',[1.5,-0.5],opts_off);

ratio = 1.1:0.01:3;
Y2 = 1.1:0.01:3;
Y1 = Y2.*ratio;
Y = [Y1', Y2'];
theta_prime = zeros(size(Y,1),2);
loglik_prime = zeros(size(Y,1),1);
opts_off=optimset('DerivativeCheck','off','Display','off','TolX',1e-6,'TolFun',1e-6,...
   'Diagnostics','off','MaxIter', 200, 'LargeScale','off');
for kk = 1: size(Y,1);
Y1 = Y(kk,1);
Y2 = Y(kk,2);
[theta_prime(kk,:),loglik_prime(kk)] = fminsearch ('two_gamma',[0,0],opts_off);
% you were not supposed to use generic optimization techniques
end
figure(1)
plot(ratio,theta_prime(:,1))
xlabel('Y1/Y2')
ylabel('\theta_1')
title('\theta_1 and Y1/Y2')









