function lnL=GaussianQuadrature(Para)
global N T X Y Z
global btaPrime_u w_u btaPrime_l w_l
Nodes = 20;
bta0 = Para(1);
sigmabta2= Para(2);
gamma =Para(3);

Li_u = ones(N,Nodes);
Li_l = ones(N,Nodes);

for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epis=-log(btaPrime_u(iNodes))*X(iT,iN)+gamma*Z(iT,iN);
            t1 = exp(-epis);
            %t1 = exp(log(btaPrime_u(iNodes))*X(iT,iN)+gamma*Z(iT,iN));
            Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
            
            epis=log(-btaPrime_l(iNodes))*X(iT,iN)+gamma*Z(iT,iN);
            t2 = exp(-epis);
            %t2 = exp(-log(-btaPrime_l(iNodes))*X(iT,iN)+gamma*Z(iT,iN));
            Li_l(iN,iNodes) = Li_l(iN,iNodes)*(1/(1+t2))^Y(iT,iN)*...
                (t2/(1+t2))^(1-Y(iT,iN));
        end
        Li_u(iN,iNodes) = Li_u(iN,iNodes)* (1/(2*pi*sigmabta2)^0.5)*exp(-(-log(btaPrime_u(iNodes))-bta0)^2/2/sigmabta2)/btaPrime_u(iNodes);
        Li_l(iN,iNodes) = Li_l(iN,iNodes)* (1/(2*pi*sigmabta2)^0.5)*exp(-(log(-btaPrime_l(iNodes))-bta0)^2/2/sigmabta2)/(-btaPrime_l(iNodes));
    end
end

Intr_u = Li_u*w_u;
Intr_l = Li_l*w_l;
lnIntr   = log(Intr_u+Intr_l);
lnL = -sum(lnIntr);
%fprintf('The likelihood is: %d when using Gaussian Quadrature.\n', lnL);
end